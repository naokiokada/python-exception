'''
イテレーターは、シーケンス(並びを扱う情報)
を新しく生成する時も利用します
ストレートに実装するといちいち違うシーケンス
にのせ替える必要が出てきます
'''
def reverse(data):
    ''' 引数に受け取ったシーケンスを逆向きにする '''
    ret = [] #このようにのせ替えるリストを生成する
    for i in range(len(data)-1, -1, -1): #要素を逆向きにアクセス
        ret.append(data[i]) #のせ替えるリストに一つずつ要素を入れる

    return ret

#reverse関数に文字列を入れる
for char in reverse('golf'):
    print(char, end=" ")