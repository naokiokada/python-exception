'''
for文の舞台裏
あまり使いませんが
イテレーターを作って、繰り返し処理を行います
'''

import sys

#通常のfor文
for i in [0, 1, 2, 3, 4, 5]:
    print(i, end=",")

print("\n", '-' * 20, "\n")

'''
リストの中身を見る
'''
l = []
#print(dir(l))
#sys.exit()

#以下のように反復子（イテレーター）を使うことも可能です
itr = iter([0, 1, 2, 3, 4, 5])
#一つずつnextでアクセス
i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

#ここでStopIterationエラーが出る
#for文はStopIterationエラーが出たら止まる
#仕組みになっている
i = next(itr)
print(i, end=",")