'''
イテレーターを改造して使う(1)
'''

#my_iterator1.pyからMyIteratorクラスを読み込む
from my_iterator1 import MyIterator

class Reverse(MyIterator):
    ''' MyIteratorを継承して更に改造 '''
    def __init__(self, data):
        #継承元のコンストラクタは使わない
        self.data = data
        #indexに文字数を入れておく
        self.index = len(data)
        #print(self.index)

    def __next__(self):
        #今回は文字数目が0だったら例外を出す
        if self.index == 0:
            raise StopIteration()
        #今度は1ずつ減らす
        self.index = self.index - 1
        return self.data[self.index]

        return value

#上記で作ったクラスReverseをインスタンス化
rev = Reverse("aiueo")
for i in rev:
    print(i, end=",")