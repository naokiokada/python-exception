'''
イテレーターを改造して使う(1)
'''
class MyIterator():
    def __init__(self, data):
        ''' コンストラクタ '''
        self.data  = data
        self.index = 0

    def __iter__(self):
        ''' selfを返すだけ '''
        return self

    def __next__(self):
        ''' nextの動作を上書きする '''
        #indexが文字数と同じになったら
        #処理しない
        if self.index == len(self.data):
            #StopIterationという例外(エラー処理)
            #を起動する
            raise StopIteration()

        #index番目の文字を取得する
        value = self.data[self.index]
        #次の文字数目にindexを移動する
        self.index = self.index + 1
        return value

if __name__ == "__main__":
    #MyIteratorをインスタンス化する
    itr = MyIterator("aiueo")
    for i in itr: #ここで自動的にnextが実行されている
        print(i, end=",")