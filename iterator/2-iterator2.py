#以下のように反復子（イテレーター）を使うことも可能です
itr = iter([0, 1, 2, 3, 4, 5])

err = '次はありません'

#一つずつnextでアクセス
i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

i = next(itr)
print(i, end=",")

#StopIterationエラーは出なくなる
i = next(itr, err)
print(i, end=",")