'''
例外の例
'''
#ValueError
a = int('a')

#IndexError
num_list = [0, 1, 1, 2, 3, 5, 8]
print(num_list[10])

#KeyError
name_rank = {1: 'sato', 2: 'suzuki', 3: 'takahashi'}
print(name_rank[4])

#ZeroDivisionError
print(100.0 / 0)

#TypeError
my_list = [0, 1, 2, 3]
my_list[0.0]