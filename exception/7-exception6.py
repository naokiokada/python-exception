import sys

class MyValueLimitError(Exception):
    '''
    独自の例外処理を扱うクラスを定義する
    '''
    

limit_number = 10000
x1 = 100
x2 = 101

try:
    x = x1 * x2
    if x > limit_number: #limit_numberより大きい場合実行
        #独自の例外クラスにraiseする
        raise MyValueLimitError(x1, x2, limit_number)
    print(x)
except MyValueLimitError as vle:
    #独自例外クラスの__str__が実行される
    print(vle)
except:
    print('Unexpected Error:', sys.exc_info())