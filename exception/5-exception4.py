import sys

    with open('test2.txt') as f:
        s = f.readline()
    print(s)
    #この後にelse節が実行される
    #ただし、returnなどで抜けると実行されない

    print('FileNotFoundError:', sys.exc_info())

    #elseはtry内の処理がexceptなく
    #実行できたらelse内の処理が実行される
    print('Read File Complete!!')
