'''
例外は、tyr except文で制御し
実行を続けることができる
'''
import sys

    #try文内に実行したい内容を入れる
    with open('test.txt') as f:
        s = f.readline()
    print(s)

    #FileNotFoundErrorの例外が起きたら
    #これが実行される
    #sys.exc_infoで実行内容表示
    print('FileNotFoundError', sys.exc_info())

    #IOErrorが起きたら実行
    print('IOError', sys.exc_info())

    #ValueErrorが起きたら実行
    print('ValueError', sys.exc_info())

    #errという変数にエラー内容を渡すことができる
    print('OSError', sys.exc_info())
    print('err', err)

    print('Unexpected Error', sys.exc_info())
