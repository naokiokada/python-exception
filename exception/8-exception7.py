'''
クリーンアップ処理
try実行後に必ず実行される
'''
import sys

def generate_intlist(x):
    test_list = []
    try:
        print('Try ' + '+' * 10)
        for i in range(x):#range(100)になる
            test_list.append(i)
            if i == 10:#10の時に例外が起動する
                raise Exception()
        print(test_list)#これは実行されない
    except Exception as inst:
        #例外実行時にこれが実行される
        print('Exception' + '+' * 10)
        print('test_list', test_list)
        print(inst)
    else:
        #これもtryが終了しないので実行されない
        print('Normal Fin' + '+' * 10)

    finally:
        #finallyはtry,exceptが処理された後に絶対処理される
        print('Finally' + '+' * 10)
        print('test_list', test_list)
        test_list.clear()
        print('test_list clear complete!!')
        print('test_list', test_list)

generate_intlist(100)#引数に100を入れる